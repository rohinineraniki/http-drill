const http = require("http");
const fs = require("fs");
const uuid = require("uuid");

function getParameters(urlParams) {
  let paramsArray = urlParams.split("/");
  if (paramsArray.length === 3) {
    let parameter = paramsArray.pop();
    return parameter;
  }
}

const server = http.createServer((request, response) => {
  try {
    if (request.url === "/html" && request.method === "GET") {
      fs.readFile("data.html", "utf-8", (error, data) => {
        if (error) {
          console.log(error);
          response.writeHead(500, "Error in reading file", {
            "content-type": "text/html",
          });
          response.end("Error in reading file");
        } else {
          response.writeHead(200, "Ok", { "content-type": "text/html" });
          response.end(data);
        }
      });
    } else if (request.url === "/json" && request.method === "GET") {
      fs.readFile("jsonData.json", "utf-8", (error, data) => {
        if (error) {
          console.log(error);
          response.writeHead(500, "Error in reading file", {
            "content-type": "text/html",
          });
          response.end("Error in reading file");
        } else {
          response.writeHead(200, "Ok", { "content-type": "application/json" });
          response.end(data);
        }
      });
    } else if (request.url === "/uuid" && request.method === "GET") {
      let generateUuid = uuid.v4();

      response.writeHead(200, "Ok", { "content-type": "application/json" });
      uuidObject = { uuid: generateUuid };

      response.write(JSON.stringify(uuidObject));
      response.end();
    } else if (request.url.startsWith("/status") && request.method === "GET") {
      const givenStatusCode = getParameters(request.url);

      let allStatusCodes = Object.keys(http.STATUS_CODES);

      if (allStatusCodes.includes(givenStatusCode)) {
        response.writeHead(
          givenStatusCode,
          http.STATUS_CODES[givenStatusCode],
          {
            "content-type": "text/html",
          }
        );

        response.end(http.STATUS_CODES[givenStatusCode]);
      } else {
        response.writeHead(404, "Not Found");

        response.end("Sorry Your request is not found");
      }
    } else if (request.url.startsWith("/delay") && request.method === "GET") {
      let userSec = getParameters(request.url);

      if (
        userSec >= 0 &&
        !isNaN(Number(userSec)) &&
        userSec < 180 &&
        userSec !== undefined
      ) {
        let secFromUrl = Number(userSec) * 1000;

        setTimeout(() => {
          response.writeHead(200, "Ok", { "content-type": "text/plain" });
          response.end("Writing response after given delay");
        }, secFromUrl);
      } else {
        response.writeHead(404, "Not Found", { "content-type": "text/plain" });
        response.end("Sorry Your request is not found");
      }
    } else {
      response.writeHead(404, "Not Found");
      response.end("Entered url is not found, Please enter correct url");
    }
  } catch (error) {
    console.log(error);
  }
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
  console.log("listening to port 3000");
});
